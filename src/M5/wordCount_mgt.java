package M5;
import java.util.Scanner;

public class wordCount_mgt {
    public static void main(String[] args) {
        wordCount_mgt();
    }

    public static void wordCount_mgt() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce una cadena: ");
        String input = sc.nextLine();
        int wordCount = input.split("\\s+").length;
        int letterCount = input.replaceAll("\\s+", "").length();
        System.out.println("Número de palabras: " + wordCount);
        System.out.println("Número de letras: " + letterCount);
    }
}


