package M5;
import java.util.Scanner;

public class findMax_mgt {
    public static void main(String[] args) {
        findMax_mgt();
    }

    public static void findMax_mgt() {
        Scanner sc = new Scanner(System.in);
        int max = Integer.MIN_VALUE;
        int num;
        do {
            System.out.println("Introduce un número (0 para terminar): ");
            num = sc.nextInt();
            if (num != 0 && num > max) {
                max = num;
            }
        } while (num != 0);
        System.out.println("El número mayor es: " + max);
    }
}

